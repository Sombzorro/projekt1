import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Aknad2 extends Application {
    double x1;
    double y1;
    double x2;
    double y2;
    Character suund;

    public static void main(String[] args) {
        launch(args);
    }
    @Override
    public void start(Stage primaryStage) {
        BorderPane piir = new BorderPane();

        Stage peaLava = new Stage();
        peaLava.setOnHiding(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                // luuakse teine lava
                Stage kusimus = new Stage();
                // küsimuse ja kahe nupu loomine
                Label label = new Label("Soovite mängust lahkuda?");
                Button mänguButton = new Button("Mängi");
                Button lahkuButton = new Button("Lahku");

                // sündmuse lisamine nupule lahku
                lahkuButton.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        kusimus.hide();
                    }
                });

                // sündmuse lisamine nupule mängi
                mänguButton.setOnAction(new EventHandler<ActionEvent>() {
                    public void handle(ActionEvent event) {
                        peaLava.show();
                        kusimus.hide();
                    }
                });

                // nuppude grupeerimine
                FlowPane pane = new FlowPane(10, 10);
                pane.setAlignment(Pos.CENTER);
                pane.getChildren().addAll(lahkuButton, mänguButton);

                // küsimuse ja nuppude gruppi paigutamine
                VBox vBox = new VBox(10);
                vBox.setAlignment(Pos.CENTER);
                vBox.getChildren().addAll(label, pane);

                //stseeni loomine ja näitamine
                Scene stseen2 = new Scene(vBox);
                kusimus.setScene(stseen2);
                kusimus.show();


            }
        }); //siin lõpeb akna kirjeldus

        ///////////////////////////////////////////////////////////////////////////////////////////


        // stseeni loomine ja näitamine
        peaLava.setTitle("Laevade pommitamine");
        Group root = new Group();
        Scene stseen = new Scene(root, Color.LIGHTBLUE);

        int ruut = 30;

        final Canvas canvas1 = new Canvas(10 * ruut, 10 * ruut);
        GraphicsContext gc1 = canvas1.getGraphicsContext2D();

        final Canvas canvas2 = new Canvas(10 * ruut, 10 * ruut);
        GraphicsContext gc2 = canvas2.getGraphicsContext2D();


        canvas1.setOnMouseClicked(event -> {
            System.out.println((int) event.getX() / 30 + "," + (int) event.getY() / 30);
        });



            canvas2.setOnMouseClicked(event1 -> {
            System.out.println((int) event1.getX() / 30 + "," + (int) event1.getY() / 30);
        });

        //Loon suvalise näidislaua
        List<ArrayList<Character>> mängulaud = new ArrayList<>();
        for (int i = 0; i < 10; i++){
            mängulaud.add(new ArrayList<>(Arrays.asList('+', '#', '~', '~', '~', '*', '*', '~', '~', 'o')));
        }
        //Kasutan mõlema mänguvälja joonistamiseks sama lauda
        laud(gc1, ruut, mängulaud);
        laud(gc2, ruut, mängulaud);

        //Objekt, mis hoiab mõlemat mänguvälja
        HBox kast = new HBox(100);
        kast.getChildren().add(canvas1);
        kast.getChildren().add(canvas2);


        root.getChildren().add(kast);
        peaLava.setScene(stseen);
        peaLava.show();


        Character suund = 'V';
        canvas1.setOnMousePressed(event -> {
            System.out.println("Alguspunkt " + event.getX() / 30 + " " + event.getY());
            this.x1 = event.getX();
            this.y1 = event.getY();
        });

        canvas1.setOnMouseReleased(event2 -> {
            System.out.println("Lõpppunkt " + event2.getX() + " " + event2.getY());
            this.x2 = event2.getX();
            this.y2 = event2.getY();
        });

    }

    //ruutude joonistamine
    private void laud(GraphicsContext gc, int ruut, List<ArrayList<Character>> staatus) {
        for (int rida = 0; rida < 10; rida++){
            for (int veerg = 0; veerg < 10; veerg++) {
                if (staatus.get(rida).get(veerg).equals('~')) { //kui on lihtsalt vesi
                    gc.setFill(Color.LIGHTBLUE);
                } else if (staatus.get(rida).get(veerg).equals('*')) { //kui on olnud möödalask
                    gc.setFill(Color.DARKBLUE);
                } else if (staatus.get(rida).get(veerg).equals('o')) {
                    gc.setFill(Color.RED); //kui on saadud pihta
                } else if (staatus.get(rida).get(veerg).equals('#')) { //kui on olnud möödalask
                    gc.setFill(Color.BLACK);
                } else if (staatus.get(rida).get(veerg).equals('+')) { //kui on olnud möödalask
                    gc.setFill(Color.GREY);
                }
                System.out.println("x " + veerg + "," + "y " + rida + " on " + staatus.get(rida).get(veerg));
                gc.fillRect(ruut*veerg,ruut*rida, ruut, ruut);
            }
        }
    }

    public void mouseDragged(MouseEvent mevent){

    }
}

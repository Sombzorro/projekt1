import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Laev {
    private Mängulaud laud1;
    private int pikkus;
    private boolean olek;
    private List<Character> laevaOsad;
    private List<ArrayList<Integer>> laevaOsadeXY;


    //Laeva lisamine mängulauale
    public Laev(int pikkus, int X, int Y, Character suund, Mängulaud laud) {
        this.pikkus = pikkus;
        this.olek = true;
        this.laud1 = laud;
        List<Character> genLaev = new ArrayList<Character>();
        for(int i = 0;i<pikkus;i++){
            genLaev.add('#');
        } this.laevaOsad = genLaev;

        List<ArrayList<Integer>> genXY = new ArrayList<ArrayList<Integer>>();

        for(int i = 0; i<pikkus;i++){
            if(suund.equals('W')){
                genXY.add(new ArrayList<Integer>(Arrays.asList(X,Y-i)));
            }
            else if (suund.equals('S')){
                genXY.add(new ArrayList<Integer>(Arrays.asList(X+i,Y)));
            }
            else if (suund.equals('E')){
                genXY.add(new ArrayList<Integer>(Arrays.asList(X,Y+i)));
            }
            else if (suund.equals('N')){
                genXY.add(new ArrayList<Integer>(Arrays.asList(X-i,Y)));
            }
        } this.laevaOsadeXY = genXY;
    }

    //Vaatab üle, kas laeval on veel terveid juppe. Kui on, siis tagastab true
    public boolean kontrolliOlek(){
        for(Character jupp : laevaOsad ){
            if(jupp.equals('#')){
                return true;
            }
        }
        return false;
    }


    /* Siia jõuab, kui laev on saanud tabamuse. Vaatab, milline konkreetne jupp sai tabamuse ja märgib selle ära karakteriga 'o'
     * Kontrollib, kas tabatud laeval on veel terveid juppe. Kui ei, siis määrab laeva uppunuks, muudab laeva 'o' -> '+'ks */
    public void jupiTabamus(int X, int Y){
       for (int i = 0; i<pikkus;i++){
               ArrayList<Integer> jupp = laevaOsadeXY.get(i);
           if(jupp.get(0) == X) {
               if (jupp.get(1) == Y) {
                   this.laevaOsad.set(i, 'o');
                   if(!kontrolliOlek()){
                       for(int g =0; g<pikkus;g++){
                           laevaOsad.set(g,'+');
                           this.olek = false;
                       }
                       System.out.println("Lasid laeva põhja!");
                   }
                   break;
               }
           }
       }
   }

   /* Kontrollib, kas paigutatav jupp mahub väljale. */ //TODO: Kontroll, kas on teise laeva kõrval või peal.
    boolean jupiKontroll(Integer X, Integer Y){
        if(X >= laud1.getSuurus() || X<0) {
            System.out.println("Jupp ei mahu merele!");
            return false;
        }
        if (Y >= laud1.getSuurus() || Y < 0) {
            System.out.println("Jupp ei mahu merele!");
            return false;
        }
        else {
            System.out.println("Jupi asukoht sobib!");
            return true;
        }
    }


    public List<ArrayList<Integer>> getLaevaOsadeXY() {
        return laevaOsadeXY;
    }

    public int getPikkus() {
        return pikkus;
    }

    public List<Character> getLaevaOsad() {

        return laevaOsad;
    }

    public boolean isOlek() {
        return olek;
    }
}
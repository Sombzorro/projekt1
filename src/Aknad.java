/*
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.util.ArrayList;
import java.util.List;

public class Aknad extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        //BorderPane piir = new BorderPane();

        Stage peaLava = new Stage();
        peaLava.setOnHiding(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                // luuakse teine lava
                Stage kusimus = new Stage();
                // küsimuse ja kahe nupu loomine
                Label label = new Label("Soovite mängust lahkuda?");
                Button mänguButton = new Button("Mängi");
                Button lahkuButton = new Button("Lahku");

                // sündmuse lisamine nupule lahku
                lahkuButton.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        kusimus.hide();
                    }
                });

                // sündmuse lisamine nupule mängi
                mänguButton.setOnAction(new EventHandler<ActionEvent>() {
                    public void handle(ActionEvent event) {
                        peaLava.show();
                        kusimus.hide();
                    }
                });

                // nuppude grupeerimine
                FlowPane pane = new FlowPane(10, 10);
                pane.setAlignment(Pos.CENTER);
                pane.getChildren().addAll(lahkuButton, mänguButton);

                // küsimuse ja nuppude gruppi paigutamine
                VBox vBox = new VBox(10);
                vBox.setAlignment(Pos.CENTER);
                vBox.getChildren().addAll(label, pane);

                //stseeni loomine ja näitamine
                Scene taustastseen = new Scene(vBox);
                kusimus.setScene(taustastseen);
                kusimus.show();
            }
        }); //siin lõpeb akna kirjeldus

        int x = 610; //siia saab panna laiuse
        int y = 610; //siia saab panna kõrguse
        int ruut = 10;
        int kaugus = ruut*10+10; // siia saab panna kauguse vasakust servast
        // TODO: siia peaks *10 asemele tulema List<ArrayList<Character>> suurus

        peaLava.setTitle("Laevade pommitamine");
        Group root = new Group();
        Canvas canvas = new Canvas(x, y);
        GraphicsContext gc = canvas.getGraphicsContext2D();
        // //TODO:lauale anname ette canvase, kui suur peab olema ruut ja listi, kus on iga ruudu jaoks väärtus olemas, et vastavalt sellele neid värvida.
        // iga kord kui mänguseis muutub laseme sellel uuesti joonistada

        laud(gc, 0, ruut, ); //laevaosade kogulist, siin näeb mängija oma laevu ja kuidas vastane neid pommitanud on
        laud(gc, kaugus, ruut, ); //vastasmängija laud, kus on näha kuhu saab sihtida, mis laevad on põhja lastud, mis pommid on mööda läinud
        root.getChildren().add(canvas);
        Scene stseen = new Scene(root, Color.LIGHTBLUE);
        stseen.setOnMouseClicked(event1 -> {
            System.out.println(event1.getX());
        });
        peaLava.setScene(stseen);
        peaLava.show();
    }

    //siin on laua joonistamise meetod

    private void laud(GraphicsContext gc, int kaugus, int ruut, List<ArrayList<Character>> staatus) {
        gc.setStroke(Color.WHITE);

        for (ArrayList<Character> rida : staatus) {
            for (Character veerg : staatus.get(staatus.indexOf(rida))) {
                if (staatus.get(staatus.indexOf(rida)).get(veerg).equals('~')) { //kui on lihtsalt vesi
                    gc.setFill(Color.LIGHTBLUE);
                }
                else if (staatus.get(staatus.indexOf(rida)).get(veerg).equals('*')) { //kui on olnud möödalask
                    gc.setFill(Color.DARKBLUE);
                }
                else if (staatus.get(staatus.indexOf(rida)).get(veerg).equals('X')) { //kui on saadud pihta
                    gc.setFill(Color.RED);
                }
                //rida on see mis muutub harvem ja peab olema y juures ja veerg koguaeg 0->koguseni ning on esimene argument
                //lahutame saadud indeksist ühe maha, et saaksime algusesse 0
                int x = staatus.indexOf(staatus.get(staatus.indexOf(staatus.get(staatus.indexOf(rida)))))-1; //veerg
                int y = staatus.indexOf(staatus.get(staatus.indexOf(rida)))-1;//rida
                gc.strokeRoundRect(x*ruut + kaugus, y*ruut, ruut, ruut, 0, 10);
            }
        }
    }

    public void mouseClicked( java.awt.event.MouseEvent  ev) {
        int x ,y ;
        x = ev.getX() ;
        y = ev.getY() ;

        ////TODO: nüüd leida selle koordinaadi järgi millisesse ruutu ta kuulub
        ////TODO: muuta selle väärtus mängulaua listis
    }
}

*/
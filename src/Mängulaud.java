import java.util.ArrayList;
import java.util.List;

public class Mängulaud {
    private int suurus;
    private List<ArrayList<Character>> laud;
    private List<Laev> lauaLaevad = new ArrayList<Laev>();
    private List<ArrayList<Integer>> laevaOsadeKogulist = new ArrayList<ArrayList<Integer>>();
    private List<ArrayList<Integer>> lastudRuudud = new ArrayList<ArrayList<Integer>>();

    /* Loob mängija soovitud suurusega laua. ~ on meri*/
    public Mängulaud(int suurus) {
        this.suurus = suurus;
        List<ArrayList<Character>> genList = new ArrayList<ArrayList<Character>>();
        for(int i = 0; i<suurus;i++) {
            genList.add(new ArrayList<Character>());
            for (int j = 0; j < suurus; j++) {
                genList.get(i).add('~');
            }
        }
        this.laud = genList;
    }


    /* Loob 10x10 standard mängulaua, kui kasutaja ei anna sisendit */
    public Mängulaud() {
        this.suurus = 10;
        List<ArrayList<Character>> genList = new ArrayList<ArrayList<Character>>();
        for(int i = 0; i<suurus;i++) {
            genList.add(new ArrayList<Character>());
            for (int j = 0; j < suurus; j++) {
                genList.get(i).add('~');
            }
        }
        this.laud = genList;
    }

    /* Genereerib originaaliga sama suure laua koopia. Igav vesi ja tühi meri */
    public List<ArrayList<Character>> tühiLaud() {
        List<ArrayList<Character>> genList = new ArrayList<ArrayList<Character>>();
        for(int i = 0; i<suurus;i++) {
            genList.add(new ArrayList<Character>());
            for (int j = 0; j < suurus; j++) {
                genList.get(i).add('~');
            }
        }
        return genList;
    }

    /* Prindib välja kogu mänguvälja. */
    public void printLaud(){
        for(ArrayList<Character> rida : laud){
            for (Character üksikRuut : rida){
                System.out.print(üksikRuut);
            }
            //uus rida
            System.out.println();
        }
        //väljade vaherida
        System.out.println();
    }

    /* Lisab isendi Laev laevade listi. Lisab kõikide selle isendi juppide koordinaadid kõikide juppide koordinaatide listi. */
    public void lisaLaev(Laev laev){
        this.lauaLaevad.add(laev);
        for(int i = 0; i < laev.getPikkus();i++) {
            laevaOsadeKogulist.add(laev.getLaevaOsadeXY().get(i));

        }
    }

    /* Teavitab mängijat, kas ta tabas või mitte. Suunab edasi lasePõhja meetodisse, mis laseb tabatud jupi põhja */
    public boolean tulista(int X, int Y){
        if(tabamus(X,Y)){
            System.out.println("Tabasid laeva");
            lasePõhja(X,Y);
            return true;
        }
        else {
            mööda(X,Y);
            System.out.println("Mööda");
            return false;
        }
    }

    /* Vaatab läbi kõik ruudud, kus on laeva juppe. Kui sihitud ruudul on mingi laev, siis tagastab true */
    public boolean tabamus(int X, int Y) {
        for (int i = 0; i < laevaOsadeKogulist.size(); i++) {
            if (laevaOsadeKogulist.get(i).get(0) == X) {
                if (laevaOsadeKogulist.get(i).get(1) == Y) {
                    return true;
                }
            }
        }
        return false;
    }

    /* Saab tabatud jupi koordinaadid. Otsib üles laeva, millele see jupp kuulub. Suunab edasi Laev.jupiTabamus juurde */
    public void lasePõhja(Integer X, Integer Y) {
        ArrayList<Integer> koordinaadid = new ArrayList<Integer>();
        koordinaadid.add(X);
        koordinaadid.add(Y);
        for (Laev laevuke : lauaLaevad) {
            if (laevuke.getLaevaOsadeXY().contains(koordinaadid)) {
                laevuke.jupiTabamus(koordinaadid.get(0), koordinaadid.get(1));
            }
        }
    }

    //Väljastab mänguvälja suuruse
    public int getSuurus() {
        return suurus;
    }

    /* #1 Joonistab kõik laevad vastavalt nende seisukorrale lauale.
       #2  Vaatame möödalasud üle */
    public List<ArrayList<Character>> joonistaLauale(){
        List<ArrayList<Character>> uusLaud = tühiLaud();

        for(Laev laevuke : lauaLaevad){
            List<ArrayList<Integer>> laevaXY = laevuke.getLaevaOsadeXY();
            List<Character> laevaJupid = laevuke.getLaevaOsad();
            //Vaatame kõik mängulaual olevad laevad üle
            for(int j = 0; j<laevuke.getPikkus();j++) {
                int õigerida = laevaXY.get(j).get(0);
                int õigeveerg = laevaXY.get(j).get(1);
                Character õigemärk = laevaJupid.get(j);
                uusLaud.get(õigerida).set(õigeveerg,õigemärk);
                ArrayList<Character> rida = uusLaud.get(õigerida);
            }
            //Vaatame üle kõik möödalasud
            for(ArrayList<Integer> koordinaadid : lastudRuudud){
                uusLaud.get(koordinaadid.get(0)).set(koordinaadid.get(1),'*');
            }
        }
        //tagastame uuendatud laua
        return uusLaud;
    }

    //Saab möödalasu koordinaadid ja jätab need meelde
    private void mööda(Integer X, Integer Y){
        ArrayList<Integer> test = new ArrayList<Integer>();
        test.add(X);
        test.add(Y);
        lastudRuudud.add(test);
    }

    public List<ArrayList<Integer>> getLaevaOsadeKogulist() {
        return laevaOsadeKogulist;
    }

    public List<Laev> getLauaLaevad() {
        return lauaLaevad;
    }

    //Määrab laua karakterite listi
    public void setLaud(List<ArrayList<Character>> laud) {
        this.laud = laud;
    }

    public List<ArrayList<Integer>> getLastudRuudud() {
        return lastudRuudud;
    }
}
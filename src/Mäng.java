import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Mäng {

    //Meetod kontrollimaks, kas sisend on arv.
    static boolean numbriKontroll(String sisend) {
            try {
                Integer.parseInt(sisend);
                System.out.println("Sisend on arv");
                return true;
            } catch (Exception e) {
                System.out.println("You fucked up");
                return false;
            }
        }

        //Meetod laeva lisamiseks. Teeb seni, kuni saab hea sisendi. //TODO: Kontrolliks, et ühelgi kohal ei oleks laeva ees, ega poleks laeva kõrval
    static public Laev lisaLaevKontroll(int pikkus, Mängulaud mängija) {
        System.out.println("Sisestad laeva pikkusega: " + pikkus);
        mängija.printLaud();
        List<Character> laevaSuunad = Arrays.asList('W', 'S', 'E', 'N');
        Scanner scan = new Scanner(System.in);
        Laev laevuke;
        boolean kontroll;
        Integer X;
        Integer Y;

        //Kordab seda tsüklit, kuni kasutaja sisestab sobiva laeva.
        while (true) {
            //Küsib seni X koordinaati, kuni see on okei
            while (true) {
                System.out.println("Laeva ahtri X-koordinaat: ");
                String sõneX = scan.nextLine();
                if (numbriKontroll(sõneX)) {
                    if (Integer.parseInt(sõneX) >= 0 && Integer.parseInt(sõneX)<mängija.getSuurus()) {
                        X = Integer.parseInt(sõneX);
                        System.out.println("mis mahub väljale.");
                        break;
                    }
                    else{
                        System.out.println("mis ei mahu väljale!");
                    }
                }
            }

            //Küsib seni Y koordinaati, kuni see on okei
            while (true) {
                System.out.println("Laeva ahtri Y-koordinaat: ");
                String sõneY = scan.nextLine();
                if (numbriKontroll(sõneY)) {
                    if (Integer.parseInt(sõneY) >= 0 && Integer.parseInt(sõneY)<mängija.getSuurus()) {
                        Y = Integer.parseInt(sõneY);
                        System.out.println("mis mahub väljale");
                        break;
                    }
                    else{
                        System.out.println("mis ei mahu väljale!");
                    }
                }
            }

            Character suund;
            //Küsib seni suunda, kuni see on okei
            do {
                System.out.println("Mis suunas laev sõidab(N,E,S,W): ");
                suund = scan.nextLine().trim().toUpperCase().charAt(0);
                if (!laevaSuunad.contains(suund)) {
                    System.out.println("Sisestasid mittesobiva suuna! ");
                }
            }
            while (!laevaSuunad.contains(suund));

            //Loob uue Laev isendi
            laevuke = new Laev(pikkus, X, Y, suund, mängija);

            kontroll = true;
            //kontrollib igat ahtrist järgnevat juppi. (Kas mahub merele)
            System.out.println(laevuke.getLaevaOsadeXY().toString());

            for (int i = 0; i < laevuke.getPikkus(); i++) {
                List<ArrayList<Integer>> laevukeseKoordinaadid = laevuke.getLaevaOsadeXY();

                if (!laevuke.jupiKontroll(laevukeseKoordinaadid.get(i).get(0), laevukeseKoordinaadid.get(i).get(1)) && kontroll) {
                    kontroll = false;
                }
            }
            //Kui kõik on OK
            if(kontroll){
                System.out.println("Laev lisatud!");
                return laevuke;
            }
            //Mingi kala
            else{
                System.out.println("Laev ei mahtunud ära, proovime uuesti.");
            }
        }
    }

    static boolean kontrolliLasku(Mängulaud player1, String sihitavRuut) {
        try {
            String Xs = sihitavRuut.split(",")[0];
            String Ys = sihitavRuut.split(",")[1];
            Integer X = Integer.parseInt(Xs);
            Integer Y = Integer.parseInt((Ys));
            if (X >= 0 && X < player1.getSuurus() && Y >= 0 && Y < player1.getSuurus()) {
                for (int i = 0; i < player1.getLastudRuudud().size(); i++) {
                    ArrayList<Integer> ruut = new ArrayList<Integer>();
                    ruut.add(X);
                    ruut.add(Y);
                    if (player1.getLastudRuudud().contains(ruut)) {
                        System.out.println("Seda sa juba proovisid.");
                        return false;
                    }
                }
                return true;
            } else {
                System.out.println("Läks vist veits nihu. Proovime uuesti, ehk saad sel korral hakkama");
            }
        } catch (Exception e) {
            System.out.println("Kindel, et õige sisendi andsid. Proovi kujul: rida,veerg. Näiteks 0,0");
        }
        return false;
    }

    //Meetod, millega mängija tulistab mingit ruutu
    static public boolean mängijaTulistab(Mängulaud player1) {
        //Küsib seni sisendit, kuni kasutaja sisestab korrektse sisendi
        Scanner scan = new Scanner(System.in);
        while (true) {
            System.out.println("Millist ruutu sa sihid? Sisesta X,Y kujul (X - rida, Y - veerg): "); //Küsib kasutajalt, kuhu ta tulistama hakkab
            String sihitavRuut = scan.nextLine();
            if (kontrolliLasku(player1, sihitavRuut)) {
                Integer X = Integer.parseInt(sihitavRuut.split(",")[0]);
                Integer Y = Integer.parseInt(sihitavRuut.split(",")[1]);
                boolean uusKatse = player1.tulista(X, Y);
                player1.setLaud(player1.joonistaLauale());
                player1.printLaud();
                return uusKatse;
            }
        }
    }

    public static void main(String[] args) {
        //laevade pikkuste list
        List<Integer> laevadePikkused = Arrays.asList(1,2,3/*,3,2,2,2,1,1,1,1*/);
        Mängulaud player1 = new Mängulaud(10);
        //Loob listi põhjal laevad
        for(Integer pikkus : laevadePikkused) {
            Laev laevuke = lisaLaevKontroll(pikkus,player1);
            player1.lisaLaev(laevuke);
            player1.setLaud(player1.joonistaLauale());
        }

         player1.printLaud();
        int laevadeArv = player1.getLauaLaevad().size();
        boolean katsetus;
        int põhjasLaevad = 0;

        //Tsükkel, mis kestab kuni laevu on veel
        while(laevadeArv != põhjasLaevad){
        //Kindlasti laseb mängijal korra tulistada
        do{
            //Tagastab true, kui mängija tabab
            katsetus = mängijaTulistab(player1);
            põhjasLaevad = 0;
            for(Laev laevuke: player1.getLauaLaevad()){
                if (!laevuke.isOlek()){
                    põhjasLaevad++;
                }
            }
        }
        while(katsetus && põhjasLaevad != laevadeArv);

            System.out.println("KÄIK LÄBI!");
            //TODO: Teine mängija tulistab
            int kontroll = player1.getLauaLaevad().size();
            for(Laev laevuke: player1.getLauaLaevad()){
                if (!laevuke.isOlek()){
                    kontroll--;
                }
            }
        }
        System.out.println("MÄNG LÄBI!");
    }
}
